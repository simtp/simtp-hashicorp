job "keycloak" {
    datacenters = [ "docker" ]
    
    group "keycloak" {
    
        task "keycloak" {
        
            driver = "docker"
            config {
                image = "jboss/keycloak:latest"
                port_map = {
                    http = 8080
                }
                
            }
            service {
                name = "keycloak"
                tags = ["http"]
            }
            resources {
                network {
                    port "http" {
                        static = 9090
                    }
                }
                memory = 756
            }
            env {
                KEYCLOAK_USER = "admin"
                KEYCLOAK_PASSWORD = "password"
            }
        }
    
    }
}