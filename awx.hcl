job "awx" {
  datacenters = ["docker"]

  group "awx" {
    task "awx_config" {
      driver = "docker"
      config {
        image = "simtp/awx-config:latest"
        volume_driver = "local"
        volumes = [ 
          "/local/config/:/var/config/"
        ]
      }
    }
    task "awx_postgres" {
      driver = "docker"
      config {
        image = "postgres:10"
        volumes = [ "postgres_data:/var/lib/postgresql/data/pgdata" ]
        volume_driver = "local"
      }
      env {
        POSTGRES_USER = "awx"
        POSTGRES_PASSWORD = "awx_postgres"
        POSTGRES_DB = "awx"
        PGDATA = "/var/lib/postgresql/data/pgdata"
      }

    }
    task "awx_memcached" {
      driver = "docker"
      config {
        image = "simtp/awx_memcached:latest"
        args  = ["-s", "/var/run/memcached/memcached.sock", "-a", "0666", "-u", "root"]
        volumes = [ "memcache_socket:/var/run/memcached/:rw"]
        volume_driver = "local"

      }
    }
  }
}

